# frozen_string_literal: true

module WaitTime
  MAX_5_SECONDS = 5
  MAX_10_SECONDS = 10
  MAX_20_SECONDS = 20
  MAX_30_SECONDS = 30
  MAX_60_SECONDS = 60
end

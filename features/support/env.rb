# frozen_string_literal: true

require 'capybara'
require 'capybara/cucumber'
require 'dotenv'
require 'minitest/spec'
require 'pry'
require 'selenium-webdriver'
require 'site_prism'

# require 'require_all'

Dotenv.load 'dev.env'

# require_all './features/pages/*/*.rb'

if ENV['TESTRUNTYPE'] == 'localChrome'
  Before do
    Capybara.current_session.current_window.resize_to(1920, 1080)
  end
  Capybara.default_driver = :chrome
  args = %w[use-fake-ui-for-media-stream --incognito]
  prefs = { 'profile.default_content_setting_values.geolocation' => 1 }
  options = Selenium::WebDriver::Chrome::Options.new(args: args)
  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
  end
else
  Capybara.default_driver = ENV.fetch('DEFAULT_DRIVER').to_sym
  CHROME_STANDALONE_URL = 'http://selenium-hub:4444/wd/hub'
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome('chromeOptions' =>
                                {
                                  args: %w[--use-fake-ui-for-media-stream use-fake-device-for-media-stream incognito],
                                  prefs => { 'profile.default_content_setting_values.geolocation' => 1 }
                                })
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.read_timeout = 120
  remote_chrome_options = {
    browser: :remote,
    url: CHROME_STANDALONE_URL,
    desired_capabilities: capabilities,
    http_client: client
  }
  Capybara.register_driver :remote_chrome do |app|
    Capybara::Selenium::Driver.new(app, remote_chrome_options)
  end
end

Capybara.configure do |config|
  # config.default_max_wait_time = 20
  config.save_path = '../hsd-watchtower/reports/'
end

Before do |_scenario|
  Capybara.current_session.current_window.resize_to(1920, 1080)
  @start_time = Time.new
  # uncomment to see all capabilities puts "\n" + Capybara.page.driver.browser.capabilities.to_json + "\n"
  puts "\nBrowser: #{Capybara.page.driver.browser.capabilities.browser_name}"
  puts "Version: #{Capybara.page.driver.browser.capabilities.version}"
  puts "Platform: #{Capybara.page.driver.browser.capabilities.platform}"
  driver_v = Capybara.page.driver.browser.capabilities.to_json.split('driverVersion').last.split(',')[0].gsub!('"', '')
  puts "driver version: #{driver_v}"
  puts "\nUsing site: #{ENV.fetch('HSD_CURRENT_HOST')} \n"
end

# After do |scenario|
#   add_test_duration_to_report(@start_time)
#   if scenario.failed?
#     puts "two browser is #{@two_browser}"
#     if @two_browser
#       Capybara.drivers.keys.select { |key| key.to_s.match(/^chrome|^remote/) }.each do |driver|
#         Capybara.current_driver = driver
#         add_screenshots_to_report(scenario)
#       end
#     else
#       add_screenshots_to_report(scenario)
#     end
#   end
#   Capybara.current_session.driver.quit
# end
#
# def add_screenshots_to_report(scenario)
#   Capybara.current_session.windows.each_with_index do |window, index|
#     Capybara.current_session.switch_to_window(window)
#     name = "#{scenario.name.gsub(/[^0-9A-Za-z\s]/, '')} #{Capybara.current_driver} tab#{index}"
#     screenshot = page.save_screenshot("#{name}.png")
#     embed screenshot.to_s, 'image/png'
#   end
# end
#
# def add_test_duration_to_report(start_time)
#   end_time = Time.new
#   test_duration_sec = (end_time.to_time.to_i - start_time.to_time.to_i).abs
#   puts "Start time: #{start_time}\n"
#   puts "End time: #{end_time}\n"
#   puts "**** Test duration: #{test_duration_sec} seconds\n"
# end

After do |scenario|
  end_time = Time.new
  test_duration_sec = (end_time.to_time.to_i - @start_time.to_time.to_i).abs
  puts "\n\n, Start time: #{@start_time}\n"
  puts "End time: #{end_time}\n"
  puts "**** Test duration: #{test_duration_sec} seconds\n"
  if scenario.failed?
    if Capybara.current_session.windows.length > 1
      Capybara.current_session.switch_to_window(Capybara.current_session.windows[0])
      name = "#{scenario.name.gsub(/[^0-9A-Za-z\s]/, '')}tab0"
      screenshot = page.save_screenshot("#{name}.png")
      embed screenshot.to_s, 'image/png'
      Capybara.current_session.switch_to_window(Capybara.current_session.windows[1])
      name = "#{scenario.name.gsub(/[^0-9A-Za-z\s]/, '')}tab1"
    else
      name = scenario.name.gsub(/[^0-9A-Za-z\s]/, '')
    end
    screenshot = page.save_screenshot("#{name}.png")
    embed screenshot.to_s, 'image/png'
  end
  Capybara.current_session.driver.quit if ENV['TESTRUNTYPE'] != 'localEdge'
end

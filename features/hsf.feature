@hsf

Feature: As a User, I should be able to search for a nova scotia service or clinician

Scenario: User should be able to view service details
  Given a user navigates to nova scotia health service directory page
  And the user is displayed with browse by links
  And a user enters text Mental Health in nova scotia search bar
  And the user clears the location field
  When the user clicks on the search button
  Then a user is displayed with: Mental Health
  And a user validates hsf service details page
    |address_label |
    |Address:      |

Scenario: User clicking on hot links displayed with searched results
  Given a user navigates to nova scotia health service directory page
  And the user is displayed with browse by links
  When the user clicks on the Pharmacies text
  And the user clicks on the search button
  Then a user is displayed with: Pharmacies
  And a user validates hsf service details page
   |address_label |
   |Address:      |

Scenario: User should be displayed with map toggle with the service searched results
  Given a user navigates to nova scotia health service directory page
  And the user is displayed with browse by links
  When a user enters text Mental Health in nova scotia search bar
  And the user clears the location field
  And the user clicks on the search button
  And a user is displayed with: Mental Health
  And a user validates hsf service details page
    |address_label |
    |Address:      |
  Then the user is displayed with map toggle

Scenario: User should be able to verify pagination after the service search
  Given a user navigates to nova scotia health service directory page
  And the user is displayed with browse by links
  When a user enters text Mental Health in nova scotia search bar
  And the user clears the location field
  And the user clicks on the search button
  And a user is displayed with: Mental Health
  And a user validates hsf service details page
    |address_label |
    |Address:      |
  Then a user verifies pagination displayed at bottom of the page

Scenario: User should be able to view service details
  Given a user navigates to nova scotia health service directory page
  And the user is displayed with browse by links
  And a user enters text Mental Health in nova scotia search bar
  And the user clears the location field
  And the user clicks on the search button
  And a user is displayed with: Mental Health
  When user clicks on first service search result
  Then user validates nova scotia service details page
    |address_label|contact_label |hours_label|phone_label|fax_label|details_label|get_directions_button|
    |Address      |Contact       |Hours      |Phone:     |Fax:     |Details      |Get directions       |
  And user verifies map is displayed
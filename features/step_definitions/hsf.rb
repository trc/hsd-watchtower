# frozen_string_literal: true

include HSFLandingPage
include WaitTime

Given(/^a user navigates to nova scotia health service directory page$/) do
  @hsf_page = HSFPage.new
  visit ENV.fetch('HSF_CURRENT_HOST')
  begin
    @hsf_page.wait_until_hsf_banner_title_visible(wait: MAX_10_SECONDS)
  rescue StandardError
    visit ENV.fetch('HSF_CURRENT_HOST')
  end
  assert_equal @hsf_page.hsf_banner_title.text, 'Health Service Finder'
  assert_equal @hsf_page.hsf_banner_description.text, 'Find health care services and self-help resources in your area
                                                       so you can quickly find the care you need.'.gsub(/\s+/, ' ')
end

When(/^a user enters text ([^\n]*) in nova scotia search bar$/) do |text|
  @hsf_page = HSFPage.new
  @hsf_page.wait_until_hsf_health_service_title_visible(wait: MAX_10_SECONDS)
  @hsf_page.hsf_health_service_title[0].assert_text('Health Service')
  @hsf_page.wait_until_hsf_health_service_description_visible(wait: MAX_10_SECONDS)
  @hsf_page.hsf_health_service_description[0].assert_text('Enter a health care service')
  @hsf_page.wait_until_hsf_health_service_keyword_search_visible(wait: MAX_10_SECONDS)
  @hsf_page.wait_until_hsf_health_service_keyword_search_visible(wait: MAX_10_SECONDS)
  @hsf_page.hsf_health_service_keyword_search.click
  @hsf_page.hsf_health_service_keyword_search.send_keys(text)
  @hsf_page.hsf_health_service_keyword_search.send_keys :enter
end

And(/^the user is displayed with browse by links$/) do
  @hsf_page = HSFPage.new
  @hsf_page.wait_until_browse_by_links_visible(wait: MAX_10_SECONDS, text: 'Or browse by...')
end

And(/^a user is displayed with: ([^\n]*)$/) do |text|
  @hsf_page = HSFPage.new
  @hsf_page.wait_until_hsf_service_tab_visible(wait: MAX_10_SECONDS, text: 'Services')
  # @hsf_page.wait_until_results_disclaimer_text_visible(wait: MAX_10_SECONDS)
  # @hsf_page.results_disclaimer_text.assert_text('Please Note: Some health services or clinics may require a scheduled
  #                                                       appointment.'.gsub(/\s+/, ' '))
  @hsf_page.search_result_text.assert_text("Showing results for #{text}")
end

Then(/^a user validates hsf service details page$/) do |data|
  @hsf_page = HSFPage.new
  @hsf_page.wait_until_hsf_service_title_visible(wait: MAX_10_SECONDS, count: 20)
  @hsf_page.hsf_service_address[1].assert_text(data.hashes.first[:address_label])
end

Then(/^the user clicks on the ([^\n]*) text$/) do |text|
  @hsf_page = HSFPage.new
  @hsf_page.browse_by_links_description.detect { |element| element.text == text }.click
  @hsf_page.wait_until_hsf_location_search_visible(wait: MAX_10_SECONDS)
  step 'the user clears the location field'
end

Then(/^the user is displayed with map toggle$/) do
  @hsf_page = HSFPage.new
  @hsf_page.wait_until_map_toggle_text_visible(wait: MAX_10_SECONDS, text: 'Show Map')
  assert_equal true, @hsf_page.map_toggle.visible?
end

Then(/^user validates nova scotia service details page$/) do |data|
  @hsf_page = HSFPage.new
  @hsf_page.service_details_page_headers[0].assert_text(data.hashes.first[:details_label])
  @hsf_page.service_details_page_headers[1].assert_text(data.hashes.first[:address_label])
  @hsf_page.service_details_page_headers[2].assert_text(data.hashes.first[:contact_label])
  @hsf_page.service_details_page_headers[3].assert_text(data.hashes.first[:hours_label])
  @hsf_page.service_details_page_description[0].assert_text(data.hashes.first[:phone_label])
  @hsf_page.service_details_page_description[1].assert_text(data.hashes.first[:fax_label])
  @hsf_page.service_details_getdirection_button.assert_text(data.hashes.first[:get_directions_button])
end

And(/^the user clears the location field$/) do
  @hsf_page.hsf_location_search.click
  clear_search_field
  assert_equal 0, @hsf_page.hsf_location_search.value.length
end

And(/^the user clicks on the search button$/) do
  @hsf_page.wait_until_hsf_health_service_search_button_visible(wait: MAX_10_SECONDS)
  @hsf_page.hsf_health_service_search_button.click
end

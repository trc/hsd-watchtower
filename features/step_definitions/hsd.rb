# frozen_string_literal: true

include HSDLandingPage
include WaitTime

Given(/^a user navigates to health service directory page$/) do
  @hsd_page = HSDPage.new
  visit ENV.fetch('HSD_CURRENT_HOST')
  begin
    @hsd_page.wait_until_banner_image_visible(wait: MAX_10_SECONDS)
  rescue StandardError
    visit ENV.fetch('HSD_CURRENT_HOST')
  end
  assert_equal @hsd_page.banner_title.text, 'Find a service'
  assert_equal @hsd_page.banner_description.text, 'Use our smart search tool to find doctors, nurse practitioners,
                                    specialists, or health services for you and your family.'.gsub(/\s+/, ' ')
  @hsd_page.wait_until_banner_image_visible(wait: MAX_10_SECONDS)
end

When(/^a user clicks on the (.*) tab$/) do |service_type|
  @hsd_page = HSDPage.new
  case service_type
  when 'service'
    @hsd_page.find_a_service_button.click
  when 'doctor'
    @hsd_page.find_a_doctor_button.click
  end
end

Given(/^a user enters doctor last name ([^"]*) in last name text field$/) do |last_name|
  step 'a user navigates to health service directory page'
  step 'a user clicks on the doctor tab'
  @hsd_page = HSDPage.new
  @hsd_page.last_name_search_field.send_keys(last_name)
end

Given(/^a user enters ([^"]*) in last name and ([^"]*) in first name text field$/) do |last_name, first_name|
  step 'a user navigates to health service directory page'
  step 'a user clicks on the doctor tab'
  @hsd_page = HSDPage.new
  @hsd_page.last_name_search_field.send_keys(last_name)
  @hsd_page.first_name_search_field.send_keys(first_name)
end

Given(/^a user enters doctor first name ([^"]*) in first name text field$/) do |first_name|
  step 'a user navigates to health service directory page'
  step 'a user clicks on the doctor tab'
  @hsd_page = HSDPage.new
  @hsd_page.first_name_search_field.send_keys(first_name)
end

And(/^a user enters text ([^"]*) in search bar$/) do |text|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_search_bar_visible(wait: MAX_60_SECONDS)
  @hsd_page.search_bar.click
  @hsd_page.search_bar.send_keys(text)
  sleep 2
  @hsd_page.wait_until_search_button_visible(wait: MAX_60_SECONDS)
  @hsd_page.search_button.click
  @hsd_page.wait_until_postal_code_search_visible(wait: MAX_60_SECONDS)
end

And(/^a ([^"]*) text is entered by user in search bar$/) do |text|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_search_bar_visible(wait: MAX_60_SECONDS)
  @hsd_page.search_bar.click
  @hsd_page.search_bar.send_keys(text)
end

Then(/^user verifies text ([^"]*) present in title of search result$/) do |search_text|
  @hsd_page = HSDPage.new
  title_text = @hsd_page.first_service_search_result[0].text
  assert(title_text.include?(search_text))
end

Then(/^user verifies title ([^"]*) is displayed in service details page$/) do |_title|
  @hsd_page = HSDPage.new
  title_text = @hsd_page.service_details_title[0].text
end

Then(/^user verifies suggestion box contains text ([^"]*) as per search$/) do |suggestion|
  @hsd_page = HSDPage.new
  suggestion_box_text = @hsd_page.search_bar_suggestions.text
  assert(suggestion_box_text.include?(suggestion))
end

Then(/^user verifies names displayed containing ([^"]*) in full name$/) do |name|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_search_result_visible(wait: MAX_10_SECONDS)
  assert @hsd_page.first_search_result[0].text, name
end

Then(/^user verifies language ([^"]*) displayed in supported language of first search result$/) do |language|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_search_result_visible(wait: MAX_20_SECONDS)
  assert @hsd_page.supported_language[0].text, language
end

Then(/^user verifies role ([^"]*) displayed in role of first search result$/) do |role|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_search_result_visible(wait: MAX_20_SECONDS)
  assert @hsd_page.doctor_role[4].text, role
end

Then(/^user verifies first service search result contains keyword and location$/) do |data|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_service_search_result_visible(wait: MAX_30_SECONDS)
  assert @hsd_page.first_service_search_result[0].text, data.hashes.first[:keyword]
  assert @hsd_page.service_search_address[0].text, data.hashes.first[:location]
end

Then(/^a user verifies ([^"]*) is displayed$/) do |filter|
  @hsd_page = HSDPage.new
  assert @hsd_page.doctor_filter.text, filter
end

Then(/^user validates service details page$/) do |data|
  @hsd_page = HSDPage.new
  @hsd_page.service_details_address[4].assert_text(data.hashes.first[:address_label])
  @hsd_page.service_details_phone[0].assert_text(data.hashes.first[:phone_label])
  @hsd_page.service_details_fax[1].assert_text(data.hashes.first[:fax_label])
  @hsd_page.header_details[0].assert_text(data.hashes.first[:details_label])
  @hsd_page.service_details_getdirection_button.assert_text(data.hashes.first[:get_directions_button])
end

And(/^user verifies map is displayed$/) do
  @hsd_page = HSDPage.new
  @hsd_page.switch_to_frame(@hsd_page.map_frame)
  if @hsd_page.service_details_map.size.positive?
    @hsd_page.wait_until_map_zoom_in_visible(wait: MAX_20_SECONDS)
    @hsd_page.wait_until_map_zoom_out_visible(wait: MAX_20_SECONDS)
    puts 'Map found'
  else
    puts 'Map not found'
  end
end

When(/^a user click on provider tab$/) do
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_provider_tab_visible(wait: MAX_20_SECONDS)
  @hsd_page.provider_tab.click
  @hsd_page.wait_until_result_visible(wait: MAX_20_SECONDS)
end

When(/^user clicks on first service search result$/) do
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_service_search_result_visible(wait: MAX_20_SECONDS)
  @hsd_page.first_service_search_result[0].click
end

And(/^a user clicks on search button$/) do
  @hsd_page = HSDPage.new
  @hsd_page.provider_search_button.click
  @hsd_page.wait_until_first_search_result_visible(wait: MAX_30_SECONDS)
end

And(/^user click on ([^"]*) search button$/) do |service_type|
  @hsd_page = HSDPage.new
  case service_type
  when 'service'
    @hsd_page.service_search_button.click
  when 'doctor'
    @hsd_page.provider_search_button.click
  end
end

And(/^user clear existing first name$/) do
  @hsd_page = HSDPage.new
  @hsd_page.first_name_clear_icon.click
end

When(/^a user applies filter: ([^"]*)$/) do |text|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_filter_title_visible(wait: MAX_10_SECONDS, text: 'Filters')
  @hsd_page = HSDPage.new
  @hsd_page.filter_option.detect { |element| element.text.include? text }.click
end

And(/^a user verifies pagination displayed at bottom of the page$/) do
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_service_pagination_visible(wait: MAX_10_SECONDS)
end

Then(/^a user verifies pagination is not displayed at bottom of the page$/) do
  @hsd_page = HSDPage.new
  assert_equal false, @hsd_page.service_pagination.visible?
end

Then(/^user verifies ([^"]*) is a role$/) do |role|
  @hsd_page = HSDPage.new
  rolevalue = @hsd_page.provider_role[0].text
  assert(rolevalue.include?(role))
end

Then(/^a user verify text ([^"]*) is displayed$/) do |filter|
  @hsd_page = HSDPage.new
  assert @hsd_page.active_filter.text, filter
end

Then(/^a user verifies location (.*) is retained$/) do |location|
  @hsd_page = HSDPage.new
  assert @hsd_page.postal_code_search.text, location
end

And(/^a user enters keyword (.*) in keyword search field$/) do |keyword|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_service_keyword_search_visible(wait: MAX_10_SECONDS)
  @hsd_page.service_keyword_search.send_keys(keyword)
end

And(/^a user enters location (.*) in location search field$/) do |location|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_postal_code_search_visible(wait: MAX_10_SECONDS)
  @hsd_page.postal_code_search.send_keys(location)
  @hsd_page.wait_until_location_first_suggestion_visible(wait: MAX_10_SECONDS)
  @hsd_page.location_first_suggestion.click
end

Then(/^a user is displayed with find a (.*) search fields$/) do |service_type|
  @hsd_page = HSDPage.new
  assert @hsd_page.service_label_text[0].text, 'Find a service'
  assert @hsd_page.service_label_text[1].text, 'Doctor'
  case service_type
  when 'service'
    @hsd_page.wait_until_service_keyword_search_visible(wait: MAX_10_SECONDS)
    @hsd_page.wait_until_postal_code_search_visible(wait: MAX_10_SECONDS)
    assert_equal @hsd_page.service_description_text.text,
                 'Search by health care service (ex. walk-in clinic, physiotherapy).'
    @hsd_page.wait_until_service_search_button_visible(wait: MAX_10_SECONDS)
  when 'doctor'
    @hsd_page.wait_until_last_name_search_field_visible(wait: MAX_10_SECONDS)
    @hsd_page.wait_until_first_name_search_field_visible(wait: MAX_10_SECONDS)
    @hsd_page.wait_until_postal_code_search_visible(wait: MAX_10_SECONDS)
    @hsd_page.wait_until_provider_search_button_visible(wait: MAX_10_SECONDS)
    assert_equal @hsd_page.service_description_text.text,
                 'Enter a last name, first name, or location to find a health care provider.'
  end
end

Given(/^a user enters less than 3 characters as service keyword$/) do
  step 'a user navigates to health service directory page'
  step 'a user is displayed with find a service search fields'
  @hsd_page.wait_until_service_keyword_search_visible(wait: MAX_10_SECONDS)
  @hsd_page.service_keyword_search.send_keys 'hh'
end

When(/^a user clicks on service search button$/) do
  @hsd_page.service_search_button.click
end

Then(/^the user is displayed with the error message: ([^\n]*)$/) do |error_message|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_error_message_visible(wait: MAX_10_SECONDS, text: error_message)
end

Then(/^the user is displayed with the service field highlighted in red$/) do
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_service_field_error_visible(wait: MAX_10_SECONDS)
end

And(/^a user filters with (.*) language$/) do |language|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_search_result_visible(wait: MAX_20_SECONDS)
  @hsd_page.wait_until_filter_language_visible(wait: MAX_20_SECONDS)
  @hsd_page.filter_language.click if @hsd_page.filter_language.text.include? language
end

And(/^a user filters with role$/) do
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_doctor_role_visible(wait: MAX_20_SECONDS)
  @hsd_page.doctor_role[4].click
end

And(/^user validates doctor details page$/) do |data|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_doctor_details_title_visible(wait: MAX_10_SECONDS)
  @hsd_page.doctor_details_title.assert_text(data.hashes.first[:title])
  @hsd_page.doctor_details[4].assert_text(data.hashes.first[:primary_address_label])
  @hsd_page.doctor_details[5].assert_text(data.hashes.first[:contact_label])
  @hsd_page.doctor_details[6].assert_text(data.hashes.first[:language_label])
  @hsd_page.service_details_getdirection_button.assert_text(data.hashes.first[:get_directions_button])
  @hsd_page.doctor_details[1].assert_text(data.hashes.first[:role_label])
end

And(/^user verifies map is displayed in doctor details page$/) do
  @hsd_page = HSDPage.new
  if @hsd_page.doctor_map.size.positive?
    puts 'Map found'
  else
    puts 'Map not found'
  end
end

And(/^user filter results as per the specialist: ([^\n]*)$/) do |text|
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_role_title_visible(wait: MAX_20_SECONDS)
  @hsd_page.doctor_specialist_label.detect { |element| element.text.include? text }.click
end

And(/^user clicks on first search result$/) do
  @hsd_page = HSDPage.new
  @hsd_page.wait_until_first_search_result_visible(wait: MAX_20_SECONDS)
  @hsd_page.first_search_result[1].click
end

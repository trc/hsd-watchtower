# frozen_string_literal: true

module HSFLandingPage
  class HSFPage < SitePrism::Page
    element :hsf_banner_title, '[data-cy="banner-title"]'
    element :hsf_banner_description, '[data-cy="banner-description"]'
    elements :hsf_health_service_title,
             '[class*="MuiTypography-root MuiTypography-body1 MuiTypography-noWrap"]'
    elements :hsf_health_service_description,
             '[class*="MuiTypography-root MuiTypography-subtitle2 MuiTypography-noWrap"]'
    element :hsf_health_service_keyword_search, '[data-cy="keyword-search"]'
    element :hsf_health_service_search_button, '[data-cy="service-search"]'
    elements :browse_by_links, '[class*="MuiGrid-root MuiGrid-container css"]'
    element :hsf_location_search, '[data-cy="location-search"]'
    elements :browse_by_links_title, '[class*="MuiTypography-root MuiTypography-h6"]'
    element :search_result_text, '[data-cy="results-found"]'
    element :results_disclaimer_text, '[data-cy="results-found-disclaimer"]'
    element :hsf_service_tab, '[aria-label="Search type"] [type="button"]'
    elements :hsf_service_title, '[data-cy="service-title"]'
    elements :hsf_service_address, '[data-cy="service-address"]'
    elements :hsf_service_phone, '[data-cy="service-phone"]'
    elements :browse_by_links_description,
             '[class*="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css"]'
    element :map_toggle_text,
            '[class*="MuiTypography-root MuiTypography-body1 MuiFormControlLabel-label css"]'
    element :map_toggle, '[class*="MuiSwitch-root MuiSwitch-sizeMedium css"]'
    elements :service_details_page_headers, '[class*="MuiTypography-root MuiTypography-h5 css"]'
    elements :service_details_page_description, '[class*="MuiTypography-root MuiTypography-h6 css"]'
    element :service_details_getdirection_button, '[data-cy="directions"]'
  end
end

def clear_search_field
  search_field = @hsf_page.hsf_location_search
  search_field_length = search_field.value.length
  backspace_key = "\u0008"
  search_field_length.times do
    search_field.send_keys backspace_key
  end
end

@hsd

Feature: As a User, I should be able to search for a service or clinician

Scenario: User should be able to find doctors with last name
  Given a user enters doctor last name Martin in last name text field
  When user click on doctor search button
  Then user verifies names displayed containing Martin in full name

Scenario: User should be able to find doctors with first name
  Given a user enters doctor first name Heidi in first name text field
  When user click on doctor search button
  Then user verifies names displayed containing Heidi in full name

Scenario: User should be able to view service details
  Given a user navigates to health service directory page
  And a user enters text healthcare in search bar
  When user clicks on first service search result
  Then user validates service details page
    |address_label|phone_label|fax_label|details_label|get_directions_button|
    |Address      |Phone:     |Fax:     |Details      |Get directions       |
  And user verifies map is displayed

Scenario: User should be able to filter doctor results
  Given a user enters doctor first name Jessica in first name text field
  When user click on doctor search button
  And a user verifies pagination displayed at bottom of the page
  Then user filter results as per the specialist: Registered Practical Nurse

Scenario: User should be able to filter service search results
  Given a user navigates to health service directory page
  And a user enters text healthcare in search bar
  And a user verifies pagination displayed at bottom of the page
  Then a user applies filter: Hospitals

Scenario: User should be able to view details of Doctor/Nurses/Specialists
  Given a user enters Martin in last name and Chelsea in first name text field
  When user click on doctor search button
  And user clicks on first search result
  Then user validates doctor details page
    |title             |primary_address_label|role_label|contact_label|language_label|get_directions_button|
    |Martin, Chelsea   |Primary address      |Role      |Contact      |Languages     |Get directions       |
  And user verifies map is displayed in doctor details page

Scenario: User should be able to search service with keyword as well as Location.
  Given a user navigates to health service directory page
  And a user enters text Healthcare in search bar
  And a user enters location Toronto in location search field
  And user click on service search button
  Then user verifies first service search result contains keyword and location
    |keyword   |location|
    |Healthcare|Toronto |

Scenario: User is able to retain city when switches from doctor/nurse practitioner to service
  Given a user navigates to health service directory page
  And a user clicks on the doctor tab
  When a user enters location Peterborough, ON, Canada in location search field
  And user click on doctor search button
  Then a user verifies location Peterborough, ON, Canada is retained

Scenario:User is able to filter result with supported language
  Given a user navigates to health service directory page
  And a user clicks on the doctor tab
  When a user enters location Peterborough, ON, Canada in location search field
  And user click on doctor search button
  And a user filters with French language
  Then user verifies language French displayed in supported language of first search result

Scenario: User should be able to filter doctor results as per the role selected
  Given a user enters doctor first name Jessica in first name text field
  When user click on doctor search button
  And a user filters with role
  Then user verifies role Medical Doctor displayed in role of first search result
#
#Scenario: User should be able to see suggestion as per text entered in search bar
#  Given a user navigates to health service directory page
#  When a Walk-In text is entered by user in search bar
#  Then user verifies suggestion box contains text Walk-In Clinic as per search

#Scenario: User should be able to see provider suggestion as per text entered in search bar
#  Given a user navigates to health service directory page
#  When a Smith text is entered by user in search bar
#  Then user verifies suggestion box contains text Alicia Vivian Smith Kartechner as per search
#
#Scenario: User should be able to see provider type suggestion as per text entered in search bar
#  Given a user navigates to health service directory page
#  And a user enters text Dentist in search bar
#  When a user click on provider tab
#  Then user verifies Dentist is a role
#
#Scenario: User should be able to search using unified search bar
#  Given a user navigates to health service directory page
#  When a user enters text Clinic in search bar
#  And user click on service search button
#  Then user verifies text Clinic present in title of search result
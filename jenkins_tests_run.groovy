#!/usr/bin/env groovy

DOCKER_IMAGE = "gcr.io/graymtn-smaug-039d666a/hsd-watchtower"
def failure = false
def status_color = "2EB886"
def status = "PASS"

if (env.feature != null && !env.feature.trim().isEmpty()) {
    feature = env.feature
}
else {
    feature = "all"
}

ansiColor('xterm') {
  def network = "hsd-watchtower.${BUILD_NUMBER}"
  node('qa') {
          attemptStage('init', { ->
    //      def filepath = "$JENKINS_HOME/hsd-watchtower-config.txt"
    //      TAG = sh (returnStdout: true, script: """
    //            grep '${env.branch_to_run}' ${filepath} | tail -1
    //        """
    //      )
    //      sh "echo ${TAG}"
    sh "echo branch name : ${env.BRANCH}"
    sh "echo branch to run: ${env.branch_to_run}"
    //  "gcr.io/graymtn-smaug-039d666a/hsd-watchtower:branch_to_run-latest"
      checkout scm
    //      DOCKER_IMAGE_WITH_TAG = "${DOCKER_IMAGE}:${TAG}"
      DOCKER_IMAGE_WITH_TAG = "${DOCKER_IMAGE}:${env.branch_to_run}"
      sh "echo DOCKER_IMAGE_WITH_TAG: ${DOCKER_IMAGE_WITH_TAG}"
      sh "docker pull $DOCKER_IMAGE_WITH_TAG"
    })
    try {
        try {
            attemptStage('run test', { ->
                echo "Setting up network and selenium..."
                sh "docker network create ${network}"
                sh "docker run -d --name selenium-${BUILD_NUMBER} --network ${network} --network-alias selenium-hub --shm-size=2g selenium/standalone-chrome:3.141.59-20210929"
                sleep 10
                echo "Creating docker container"
                sh "docker run -dit --name rakebuild-${BUILD_NUMBER} --env-file ${env.env_value}.env -e RAILS_ENV=test --network ${network} ${DOCKER_IMAGE_WITH_TAG} bash"
                sh "docker exec rakebuild-${BUILD_NUMBER} mkdir reports"
                echo "Running tests..."
                sh "docker exec rakebuild-${BUILD_NUMBER} bundle exec rake ${feature}"
        }) } catch (Exception e) {
                failure = true
                status = "FAIL"
                throw e
            } finally {
                echo "Copying Report..."
               // sh "docker exec rakebuild-${BUILD_NUMBER} ls"
               // sh "docker exec rakebuild-${BUILD_NUMBER} bash -c 'cd reports && ls'"
               // sh "docker exec rakebuild-${BUILD_NUMBER} pwd"
                sh "docker cp rakebuild-${BUILD_NUMBER}:/usr/src/hsd-watchtower/reports ./"
                echo "Publishing Report..."
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'reports', reportFiles: "report_${feature}.html", reportName: 'HSD-Watchtower Report', reportTitles: ''])
                if (env.slack_send == "TRUE") {
                    echo "Sending Slack msg..."
                    if(failure == false) {
                        status_color = '#2EB886'
                        status = "PASS"
                    } else {
                        status_color = '#ff0000'
                        status = "FAIL"
                    }
                    slackSend channel: "hsd-automation",
                        message: "HSD-Watchtower Build Details: \n ${env.JOB_NAME} Build# ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>) \n" +
                        "Feature: Rake ${feature} \n" +
                        "Automation Report: ${env.build_url}/HSD-Watchtower_Report/ " +
                        "\n Status: ${status} \n",
                        teamDomain: "thinkresearch.slack.com",
                        color: "${status_color}",
                        tokenCredentialId: "slack-token"
                }
            }
    } catch (Exception e) {
            throw e
    } finally {
        attemptStage('cleanup', { ->
            echo "Cleanup..."
            sh "docker stop selenium-${BUILD_NUMBER} || true"
            sh "docker rm selenium-${BUILD_NUMBER} || true"
            sh "docker rm -f rakebuild-${BUILD_NUMBER} || true"
            sh "docker network rm ${network}"
            echo "Cleaning up jenkins workspace directory - report html and png files"
            def screenshot_exist = sh script: 'ls reports/ -1 | grep .png | wc -l | tr -d " \t\n\r"', returnStdout:true
            echo "screenshot exist: ${screenshot_exist}"
            if (screenshot_exist != "0") {
                echo "removing screenshots"
                sh "rm reports/*.png"
             }
            def report_exist = sh script: 'ls reports/ -1 | grep .html | wc -l | tr -d " \t\n\r"', returnStdout:true
            echo "report exist: ${report_exist}"
            if (report_exist != "0") {
                echo "removing report"
                sh "rm reports/*.html"
            }
        })
    }
  }
}

def attemptStage(String stageName, Closure steps) {
  stage(stageName) {
    try {
      steps()
    } catch (Exception e) {
      echo "Error occurred: ${e}"
      failure = true
      status = "FAIL"
      throw e
    }
  }
}

# HealthServiceDirectory Watchtower
----
## E2E Automated test suite

### Dependencies
- Docker >= 18.03-ce

### Setup
1. Clone this repo
2. Create a .env file with the required [environment variables](#Environment variables)
3. Download Rail Installer
4. Run bundle install
5. Build the image
6. Run the image and desired profile

### Build the image
1.  Obtain the username and password for accessing https://gems.gcp.trchq.com.
    (More information can be found about our gem server architecture at https://bitbucket.org/trc/trc-gemserver)
2.  Replace username and password and run the command:
    "docker build -t hsd-watchtower --build-arg GEM_CREDENTIALS={{username:password}} ."
    (note the period at the end is needed)

### Setup docker network and selenium Chrome node
1. Create a docker network:
    docker network create hsd-watchtower
2. Start up selenium Chrome node:
    docker run -d --network hsd-watchtower --network-alias selenium-hub --shm-size=2g selenium/standalone-chrome:4.1.3-20220405
3. Run the automation with either docker container setup or local usage setup

### Run the automation - Docker container Usage
To run in docker container:
1. If you are not running the automation locally, open .dockerignore file and remove the line reports/
    (you should re-add before checking in)
2. docker run -it --rm --network hsd-watchtower --env-file environmentname.env hsd-watchtower rake all

### Run the automation - Local usage (allows visual inspection of the automation process)
To run locally:
1. Download Chromedriver
2. Install locally
3. Add chromedriver installation path to windows environment variables
    (system control panel -> advanced -> environment variables -> system variables ->  path)
4. In cucumber.yml you can set which feature file to run locally by modifying the --tags value in the "Local:" line.  
    change the --tags value to the desired feature tag
    ex. to change from user login to account settings
         "local: -f pretty -f html -o reports/local.html --tags @user_login LOCAL=true"
         "local: -f pretty -f html -o reports/local.html --tags @account_settings LOCAL=true"
5. rake local

If you plan to visually inspect the automation process, remember to clone the repo in your machine directly, not inside any vagrant box.

### Environment variables
The following environment variables are available.

#### Required
| Name | Default | Description |
|------|---------|-------------|
| VC_PROVIDER_USERNAME | username@thinkresearch.com | Virtual Care provider email address |
| VC_PROVIDER_PASSWORD | userpassword | Virtual Care provider password |
| VC_PATIENT_USERNAME | username@thinkresearch.com | Virtual Care patient email address |
| VC_PATIENT_PASSWORD | userpassword | Virtual Care patient password |
| VC_ADMIN_USERNAME | username@thinkresearch.com | Virtual Care admin email address |
| VC_ADMIN_PASSWORD | userpassword | Virtual Care admin password |
| VC_SUPERUSER_USERNAME | username@thinkresearch.com | Virtual Care superuser email address |
| VC_SUPERUSER_PASSWORD | userpassword | Virtual Care superuser email address |

#### Optional
| Name | Default | Description |
|------|---------|-------------|
| DEFAULT_DRIVER | remote_chrome | Web driver for running scenarios |
| VC_LOCAL_HOST | http://patientos.ep.local.trchq.com:3001 | null |
| VC_DEV_HOST | https://virtualcare.dev.gcp.trchq.com | null |
| VC_STG_HOST | https://virtualcare.stg.gcp.trchq.com | null |
| VC_CURRENT_HOST | https://virtualcare.dev.gcp.trchq.com | null |
| UNAUTHORIZED_AFTER_LOGOUT | /appointments | null |
| VC_PROVIDER_DISPLAYNAME | userdisplayname | null |
| VC_PATIENT_DISPLAYNAME | userdisplayname | null |
| VC_SUPERUSER_DISPLAYNAME | userdisplayname | null |

----
### Reports
----
After running rake, there is a log called report.html generated with results of the tests.

----
### Changelog
* 12-Mar-2018 First commit
* 20-Mar-2018 Introducing Dockerfile and Jenkinsfile
* 03-Jan-2019 Updated instructions for local usage to include installing chromedriver and adding env path,
added bundle install step, added step to remove /reports for docker container usage

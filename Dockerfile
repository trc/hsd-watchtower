FROM ruby:2.6

ARG GEM_CREDENTIALS

WORKDIR /usr/src/hsd-watchtower

RUN bundle config http://gems.gcp.trchq.com
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install --jobs 8
COPY . .